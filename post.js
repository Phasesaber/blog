$(document).ready(function(){
	$("#stick").sticky({topSpacing:0});			
	update();
});

function update(){
	if(window.location.hash !== ""){
		$(".post").hide();
		$(window.location.hash).show();
		$(".main").show();
		$(".peak").hide();
	}else{
		$(".post").show();
		$(".main").hide();
		$(".peak").show();
	}
}

window.onhashchange = function(){
        update()
}
